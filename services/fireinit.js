import firebase from 'firebase'
// import 'firebase/auth/dist/index.cjs'
// import 'firebase/firestore/dist/index.cjs'
// import 'firebase/database/dist/index.cjs'

// Initialize Firebase
var config = {
apiKey: "AIzaSyDWwxkV-m3Rs-cbaqPF5PJJ2RMON8AsxzI",
authDomain: "apascentai-1a299.firebaseapp.com",
databaseURL: "https://apascentai-1a299.firebaseio.com",
projectId: "apascentai-1a299",
storageBucket: "apascentai-1a299.appspot.com",
messagingSenderId: "418953745704"
};

!firebase.apps.length ? firebase.initializeApp(config) : firebase.app();
// !firebase.apps.length ? firebase.initializeApp(config) : ''
export const GoogleProvider = new firebase.auth.GoogleAuthProvider()
export const auth = firebase.auth()
export const DB = firebase.database()
export const StoreDB = firebase.firestore()

StoreDB.settings({ timestampsInSnapshots: true }); 
export default firebase