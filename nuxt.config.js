const resolve = require('path').resolve
module.exports = {
  /*
  ** Headers of the page
  */
  css: [
    // SCSS file in the project
    '@/assets/scss/main.scss'
  ],
  head: {
    title: 'Apascentai',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Devocional Diário para os grupos pequenos no modelo GEMES' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css?family=Karma:300,400,500,600,700' },
      { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/icon?family=Material+Icons' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#6D81C7', height: '5px', continuous: true },
  /*
  ** Build configuration
  */
  build: {
    modules: [
      'bootstrap-vue/nuxt',  
    ],
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
      const vueLoader = config.module.rules.find((loader) => loader.test.toString() === '/\\.vue$/')
      vueLoader.options.loaders.scss =
        [
          'vue-style-loader',
          'css-loader',
          // 'postcss-loader',
          'sass-loader',
          {
            loader: 'sass-resources-loader',
            options: {
              resources: resolve(__dirname, './assets/scss/main.scss')
            }
          }
        ]
    }
  },
  env: {
    baseUrl: process.env.BASE_URL || 'https://apascentai-1a299.firebaseio.com/',
    fbAPIKey: 'AIzaSyDWwxkV-m3Rs-cbaqPF5PJJ2RMON8AsxzI'
  },
  // router: {
  //    middleware: 'router-auth'
  // },
  plugins: [
    '~/plugins/fireauth.js'
  ],
  modules: [
    '@nuxtjs/axios',
  ],
  axios: {
    baseURL: process.env.BASE_URL || 'https://apascentai-1a299.firebaseio.com/',
    credentials: false
  },
  footer:{
    script:[
      {src: '//unpkg.com/babel-polyfill@latest/dist/polyfill.min.js'},
    ]
  }
}
