import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const _72e2e86c = () => import('..\\pages\\perfil.vue' /* webpackChunkName: "pages_perfil" */).then(m => m.default || m)
const _3b4d826c = () => import('..\\pages\\devocional.vue' /* webpackChunkName: "pages_devocional" */).then(m => m.default || m)
const _46b0a899 = () => import('..\\pages\\biblia\\index.vue' /* webpackChunkName: "pages_biblia_index" */).then(m => m.default || m)
const _0b8d8dc0 = () => import('..\\pages\\biblia\\livros\\livro.vue' /* webpackChunkName: "pages_biblia_livros_livro" */).then(m => m.default || m)
const _d3b7a13c = () => import('..\\pages\\index.vue' /* webpackChunkName: "pages_index" */).then(m => m.default || m)



if (process.client) {
  window.history.scrollRestoration = 'manual'
}
const scrollBehavior = function (to, from, savedPosition) {
  // if the returned position is falsy or an empty object,
  // will retain current scroll position.
  let position = false

  // if no children detected
  if (to.matched.length < 2) {
    // scroll to the top of the page
    position = { x: 0, y: 0 }
  } else if (to.matched.some((r) => r.components.default.options.scrollToTop)) {
    // if one of the children has scrollToTop option set to true
    position = { x: 0, y: 0 }
  }

  // savedPosition is only available for popstate navigations (back button)
  if (savedPosition) {
    position = savedPosition
  }

  return new Promise(resolve => {
    // wait for the out transition to complete (if necessary)
    window.$nuxt.$once('triggerScroll', () => {
      // coords will be used if no selector is provided,
      // or if the selector didn't match any element.
      if (to.hash) {
        let hash = to.hash
        // CSS.escape() is not supported with IE and Edge.
        if (typeof window.CSS !== 'undefined' && typeof window.CSS.escape !== 'undefined') {
          hash = '#' + window.CSS.escape(hash.substr(1))
        }
        try {
          if (document.querySelector(hash)) {
            // scroll to anchor by returning the selector
            position = { selector: hash }
          }
        } catch (e) {
          console.warn('Failed to save scroll position. Please add CSS.escape() polyfill (https://github.com/mathiasbynens/CSS.escape).')
        }
      }
      resolve(position)
    })
  })
}


export function createRouter () {
  return new Router({
    mode: 'history',
    base: '/',
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    scrollBehavior,
    routes: [
		{
			path: "/perfil",
			component: _72e2e86c,
			name: "perfil"
		},
		{
			path: "/devocional",
			component: _3b4d826c,
			name: "devocional"
		},
		{
			path: "/biblia",
			component: _46b0a899,
			name: "biblia"
		},
		{
			path: "/biblia/livros/livro",
			component: _0b8d8dc0,
			name: "biblia-livros-livro"
		},
		{
			path: "/",
			component: _d3b7a13c,
			name: "index"
		}
    ],
    
    
    fallback: false
  })
}
